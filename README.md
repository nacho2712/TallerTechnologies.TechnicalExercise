You are given two arbitrarily large numbers,
stored one digit at a time in an array.
The first must be added to the second,
and the second must be reversed before addition.
The goal is to calculate the sum of those two sets of values.

IMPORTANT NOTE:
- The input can be any length (i.e: it can be 20+ digits long).
- num1 and num2 can be different lengths.

Sample Inputs:
num1 = 123456
num2 = 123456

Sample Output:
Result: 777777
Please include a demonstration of appropriate unit tests for this functionality.