﻿using TallerTechnologies.Exercise.Backend.Services;
using System;

namespace TallerTechnologies.Exercise.Backend
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initailize service modules
            ISetCalculator setCalculator = new SetsCalculator();

            var enabled = true;
            while (enabled)
            {
                Console.WriteLine("Insert Num 1:");
                var num1 = Console.ReadLine();

                Console.WriteLine("Insert Num 2:");
                var num2 = Console.ReadLine();
                try
                {
                    var result = setCalculator.GetSumOfSetsNumbers(num1, num2);
                    Console.WriteLine("Result: ", result);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.ToString());
                }
            }
        }


    }
}
