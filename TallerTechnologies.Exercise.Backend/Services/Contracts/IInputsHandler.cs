﻿namespace TallerTechnologies.Exercise.Backend
{
    public interface IInputsHandler
    {
        /// <summary>
        /// Validate if the string is a right number
        /// </summary>
        /// <param name="number"></param>
        bool ValidateIfStringIsNumber(string number);
        
        /// <summary>
        /// Convert string to set of numbers
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        short[] ParseStringToArrayOfNumbers(string number);

        /// <summary>
        /// Get the result of sum th
        /// </summary>
        /// <param name="num1"></param>
        /// <param name="num2"></param>
        /// <returns></returns>
        string GetTheSumOfSetsNumbers(short[] num1, short[] num2);
    }
}
