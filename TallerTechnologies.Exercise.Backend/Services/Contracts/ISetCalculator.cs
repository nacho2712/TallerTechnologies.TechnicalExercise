﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TallerTechnologies.Exercise.Backend.Services
{
    public interface ISetCalculator
    {
        string GetSumOfSetsNumbers(string num1, string num2);
    }
}
