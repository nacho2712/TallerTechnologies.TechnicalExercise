﻿using System;
using System.Collections.Generic;

namespace TallerTechnologies.Exercise.Backend
{
    public class InputsHandler : IInputsHandler
    {
        #region Validator
        public bool ValidateIfStringIsNumber(string number)
        {
            var a = int.TryParse(number, out int num);
            return a;
        }
        #endregion

        #region Parser
        public short[] ParseStringToArrayOfNumbers(string numbersString)
        {
            List<short> setNumbers = new List<short>();

            foreach (var number in numbersString.ToCharArray())
            {
                setNumbers
                    .Add(short.Parse(number.ToString()));
            }

            return setNumbers.ToArray();
        }
        #endregion

        #region Calculate
        public string GetTheSumOfSetsNumbers(short[] num1, short[] num2)
        {
            // Before addition, reverse set num2
            num2 = GetReversedSet(num2);

            // Get min length to do the first sum (if both length are equal, then this will be the final result)
            var minLength = num1.Length <= num2.Length ? num1.Length : num2.Length;

            // Get final result
            var result = SumSets(num1, num2, minLength);

            // Validate if the length are not equal
            if (num1.Length != num2.Length)
            {
                // Validate what is the longer set 
                // And add the rest of the set to the end of the result list
                if (num1.Length > num2.Length)
                {
                    AddRestOfSetToResult(result, num1, num2.Length);
                }
                else
                {
                    AddRestOfSetToResult(result, num2, num1.Length);
                }
            }

            // Concat every number in a string.
            return String.Join("", result);
        }

        private short[] GetReversedSet(short[] num)
        {
            var reversedSet = new List<short>();
            for (int i = num.Length - 1; i >= 0; i--)
            {
                reversedSet.Add(num[i]);
            }

            return reversedSet.ToArray();
        }
        
        private List<short> SumSets(short[] num1, short[] num2, int endIndex)
        {
            var result = new List<short>();

            for (int i = 0;i < endIndex;i++)
            {
                var number = num1[i] + num2[i];
                result.Add((short)number);
            }

            return result;
        }

        private void AddRestOfSetToResult(List<short> finalResult, short[] num, int startIndex)
        {
            for (int i = startIndex; i < num.Length; i++)
            {
                finalResult.Add((short)num[i]);
            }
        }
        #endregion
    }
}
