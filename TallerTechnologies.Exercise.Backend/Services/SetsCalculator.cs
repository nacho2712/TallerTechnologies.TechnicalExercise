﻿using System;
using System.Linq;

namespace TallerTechnologies.Exercise.Backend.Services
{
    public class SetsCalculator : ISetCalculator
    {
        IInputsHandler _inputsHandler;

        public SetsCalculator()
        {
            // Simulate dependency injection
            _inputsHandler = new InputsHandler();
        }

        public string GetSumOfSetsNumbers(string num1, string num2)
        {
            // Clean up whitespace from strings.
            num1 = String.Concat(num1.Where(x => !Char.IsWhiteSpace(x)));
            num2 = String.Concat(num2.Where(x => !Char.IsWhiteSpace(x)));

            // Validate if some of the sets are wrong
            if (!_inputsHandler.ValidateIfStringIsNumber(num1) || !_inputsHandler.ValidateIfStringIsNumber(num2))
            {
                return "Wrong number. Try Again!";
            }

            // Convert strings inputs to an arrays of numbers
            var setNum1 = _inputsHandler.ParseStringToArrayOfNumbers(num1);
            var setNum2 = _inputsHandler.ParseStringToArrayOfNumbers(num2);

            // Get result of sum
            return _inputsHandler.GetTheSumOfSetsNumbers(setNum1, setNum2);
        }
    }
}
