﻿using TallerTechnologies.Exercise.Backend;
using Xunit;

namespace UniTests
{
    public class InputsHandlerTests
    {
        IInputsHandler _inputsHandler;

        public InputsHandlerTests()
        {
            _inputsHandler = new InputsHandler();
        }

        [Theory]
        [InlineData("123", true)]
        [InlineData("12a3", false)]
        [InlineData("123_", false)]
        [InlineData("123 ", true)]
        public void NumberValidatorTest(string num, bool expectedResult)
        {
            var result = _inputsHandler.ValidateIfStringIsNumber(num);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData("123", new short[] { 1, 2, 3 })]
        [InlineData("6504", new short[] { 6, 5, 0, 4 })]
        [InlineData("1", new short[] { 1 })]
        public void ParseStringToArrayOfNumbersTest(string num, short[] expectedResult)
        {
            var result = _inputsHandler.ParseStringToArrayOfNumbers(num);
            Assert.Equal(expectedResult, result);
        }

        [Theory]
        [InlineData(new short[] { 1, 2, 3 }, new short[] { 1, 2, 3 }, "444")]
        [InlineData(new short[] { 6,5,8,2,1 }, new short[] { 2,6,5 }, "11111021")]
        [InlineData(new short[] { 6, 5 }, new short[] { 1,1,1,0,5,4,7}, "13950111")]
        [InlineData(new short[] { 6 }, new short[] { 1, 1, 1, 0, 5, 4, 7 }, "13450111")]
        public void GetTheSumOfSetsNumbersTest(short[] num1, short[] num2, string expectedResult)
        {
            var result = _inputsHandler.GetTheSumOfSetsNumbers(num1, num2);
            Assert.Equal(expectedResult, result);
        }
    }
}
