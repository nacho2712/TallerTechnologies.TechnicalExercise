using TallerTechnologies.Exercise.Backend.Services;
using Xunit;

namespace UniTests
{
    public class SetsCalculatorTests
    {
        ISetCalculator _setCalculator;

        public SetsCalculatorTests()
        {
            _setCalculator = new SetsCalculator();
        }

        [Theory]
        [InlineData("123", "321", "246")]
        [InlineData("123", "123", "444")]
        [InlineData("56", "746566810", "578665647")]
        [InlineData("120548787", "6548", "9651148787")]
        [InlineData("12  3", "32 1 ", "246")]
        [InlineData("  123", "1 23", "444")]
        [InlineData("56", "7 465 66810", "578665647")]
        [InlineData("120 548787", "6548", "9651148787")]
        [InlineData("120A548787", "6548", "Wrong number. Try Again!")]
        [InlineData("120548787", "65S48", "Wrong number. Try Again!")]
        [InlineData("12054878AS7", "65S48", "Wrong number. Try Again!")]
        [InlineData("12054878AS7", "", "Wrong number. Try Again!")]
        [InlineData("", "65S48", "Wrong number. Try Again!")]
        public void GetSumOfSetsNumbersTests(string num1, string num2, string expectedResult)
        {
            var result = _setCalculator.GetSumOfSetsNumbers(num1, num2);
            Assert.Equal(expectedResult, result);
        }
    }
}
